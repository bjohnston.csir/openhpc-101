# 6. Resource Execution

## 6.0. Introduction to Chapter

## 6.1. Resource Manager Startup

>[!VIDEO]
> Start of video: [6.01 - Resource Manager Startup](https://www.youtube.com/watch?v=EbZ02NxRjBk&list=PL2s6Yr_Iu_kfOmhsxBSstz00p10uOo9eY&index=11)

Start MUNGE and Slurm controller on master host:

```bash
[root@sms-host vagrant]# systemctl enable munge
[root@sms-host vagrant]# systemctl enable slurmctld
[root@sms-host vagrant]# systemctl start munge
[root@sms-host vagrant]# systemctl start slurmctld
```
Start Slurm clients on compute hosts:
```bash
[root@sms-host vagrant]# pdsh -w compute0[0-1] systemctl start slurmd
```

## 6.2. Prepare for a Test Job

>[!VIDEO]
> Start of video: [6.02 - Prepare for a Test Job](https://www.youtube.com/watch?v=EZ-IRkK9r7I&list=PL2s6Yr_Iu_kfOmhsxBSstz00p10uOo9eY&index=12)

Add a “test” user on SMS host for running an example job

```bash
[root@sms-host vagrant]# useradd -m test
```

 Synchronise new user credentials across the cluster using xCAT’s `xdcp` merge functionality, which adds new entries into credential files on compute nodes.

```bash
# Create a sync file for pushing user credentials to the nodes
[root@sms-host vagrant]# echo "MERGE:" > syncusers  
[root@sms-host vagrant]# echo "/etc/passwd -> /etc/passwd" >> syncusers  
[root@sms-host vagrant]# echo "/etc/group -> /etc/group" >> syncusers  
[root@sms-host vagrant]# echo "/etc/shadow -> /etc/shadow" >> syncusers
```
Use xCAT to distribute credentials to nodes:

```bash
[root@sms-host vagrant]# xdcp compute -F syncusers
```

> [!TIP]
> An alternative approach that achieves similar results to `xdcp` is: `updatenode compute -F`, which copies all files defined within the `synclist` definition, located at `/install/custom/netboot/compute.synclist`, to the target compute nodes (see **xCAT Setup**).

## 6.3. Interactive Test Job

>[!VIDEO]
>Start of video: [6.03 - Interactive Test Job](https://www.youtube.com/watch?v=DoIrav6G-Hg&list=PL2s6Yr_Iu_kfOmhsxBSstz00p10uOo9eY&index=13)

- The following makes use of OpenHPC’s `prun` which is a companion job-launch utility that abstracts job launch across different resource managers and MPI stacks across the OpenHPC ecosystem of various environments.

Switch to "test" user:

```bash
[root@sms-host vagrant]# su - test
```

Compile MPI "hello world" example:

```bash
[test@sms-host ~]$ mpicc -O3 /opt/ohpc/pub/examples/mpi/hello.c
```

Submit interactive job request and use `prun` to launch executable:
```bash
[test@sms-host ~]$ srun -n 8 -N 2 --pty /bin/bash
[test@compute00 ~]$ prun ./a.out
```

The **output** of the `prun` command should roughly look as follows:

```text
[prun] Master compute host = compute00
[prun] Resource manager = slurm
[prun] Launch cmd = mpirun ./a.out (family=openmpi3)

Hello, world (8 procs total)
--> Process #   1 of   8 is alive. -> compute00.hpcnet
--> Process #   2 of   8 is alive. -> compute00.hpcnet
--> Process #   3 of   8 is alive. -> compute00.hpcnet
--> Process #   0 of   8 is alive. -> compute00.hpcnet
--> Process #   5 of   8 is alive. -> compute01.hpcnet
--> Process #   6 of   8 is alive. -> compute01.hpcnet
--> Process #   7 of   8 is alive. -> compute01.hpcnet
--> Process #   4 of   8 is alive. -> compute01.hpcnet
```

Afterwards, you can exit back to your root user:

```bash
[test@compute00 ~]$ exit
[test@sms-host ~]$ exit
[root@sms-host vagrant]#
```

> [!CONGRATS]
> You have successfully run an interactive job on your cluster! This means that users of the cluster can now successfully run their own interactive jobs on the cluster through the system scheduler, Slurm.

## 6.4. Batch Execution

>[!VIDEO]
>Start of video: [6.04 - Batch Execution](https://www.youtube.com/watch?v=pkuZxr8laZA&list=PL2s6Yr_Iu_kfOmhsxBSstz00p10uOo9eY&index=14)

- The following makes use of OpenHPC’s `prun` which is a companion job-launch utility that abstracts job launch across different resource managers and MPI stacks across the OpenHPC ecosystem of various environments.

Switch to "test" user:
```bash
[root@sms-host vagrant]$ su - test
```
Copy example job script:
```bash
[test@sms-host ~]$ cp /opt/ohpc/pub/examples/slurm/job.mpi .
```
Examine contents (and edit to set desired job sizing characteristics):
```bash
[test@sms-host ~]$ cat job.mpi
```

In the `job.mpi` file, you should see the following:

```text
#!/bin/bash
#SBATCH -J test         # Job name
#SBATCH -o job.%j.out   # Name of stdout output file (%j expands to %jobId)
#SBATCH -N 2            # Total number of nodes requested
#SBATCH -n 16           # Total number of mpi tasks #requested
#SBATCH -t 01:30:00     # Run time (hh:mm:ss) - 1.5 hours

# Launch MPI-based executable
prun ./a.out  
```

Launch the batch job using the following command:

```bash
[test@sms-host ~]$ sbatch job.mpi
```

You should see the following:

```text
Submitted batch job 33
[prun] Resource manager = slurm
[prun] Launch cmd = mpirun ./a.out (family=openmpi3)

Hello, world (8 procs total)
    --> Process #   1 of   8 is alive. -> compute00.hpcnet
    --> Process #   2 of   8 is alive. -> compute00.hpcnet
    --> Process #   3 of   8 is alive. -> compute00.hpcnet
    --> Process #   0 of   8 is alive. -> compute00.hpcnet
    --> Process #   5 of   8 is alive. -> compute01.hpcnet
    --> Process #   6 of   8 is alive. -> compute01.hpcnet
    --> Process #   7 of   8 is alive. -> compute01.hpcnet
    --> Process #   4 of   8 is alive. -> compute01.hpcnet
```

Afterwards, you can exit back to your root user:

```bash
[test@compute00 ~]$ exit
[test@sms-host ~]$ exit
[root@sms-host vagrant]#
```
> [!CONGRATS]
> You have successfully run a Batch submission job on your cluster! This means that users of the cluster can now successfully submit their own jobs for processing and executing on the cluster through the system scheduler, Slurm.
