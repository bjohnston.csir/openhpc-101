# 3. xCAT Setup

## 3.0. Chapter Overview 

This section will guide you through the setup of **xCAT** on the **SMS host**. 

In the previous chapter, the SMS host was deployed using a Vagrant definition file to ensure system parity and correctness. The basic OpenHPC management framework was installed.

> [!NOTICE]
> Anticipated time to complete this chapter: 120 to 180 minutes.

> [!TIP]
> You may wish to consider running snapshots of the SMS host at various milestones to ensure you are able to quickly recover from any major bugs.<BR>

> [!NOTICE]
> The download of the CentOS 7 DVD-1908 iso is 4.3GB and may take a significant amount of time.

## 3.1. Complete basic xCAT setup

> [!VIDEO]
> Start of video [3.01 - HPC Ecosystems OpenHPC101 Complete basic xCAT setup](https://www.youtube.com/watch?v=0KMlhiE2-z0&list=PL2s6Yr_Iu_ke16_di1C3dowXHF-hRLmC9&index=10)

All of the packages necessary to use xCAT on the SMS host should be installed. It is now time to enable support for local provisioning using a second private interface (as defined in **[Figure 1](1-getting_ready#fig1)** with `${sms_eth_internal}` and `${sms_ip}`, which by default is `eth1` and `10.10.10.10/24`).

```bash
[root@sms-host vagrant]# ifconfig ${sms_eth_internal} ${sms_ip} netmask ${internal_netmask} up
[root@sms-host vagrant]# chdef -t site dhcpinterfaces="xcatmn|${sms_eth_internal}"

# OUTPUT: 1 object definitions have been created or modified.
```

## 3.2. Define compute image for provisioning

> [!VIDEO]
> Start of video [3.02A - HPC Ecosystems OpenHPC101 - Define compute image for provisioning](https://www.youtube.com/watch?v=p7ksrSa6Zmo&list=PL2s6Yr_Iu_ke16_di1C3dowXHF-hRLmC9&index=11)

If the CentOS 7.7 1908 DVD ISO image is not already available, download the ISO image [https://vault.centos.org/7.7.1908/isos/x86_64/](https://vault.centos.org/7.7.1908/isos/x86_64/)
> [!NOTICE]
> [**CentOS 7.7 1908 DVD ISO**](https://vault.centos.org/7.7.1908/isos/x86_64/CentOS-7-x86_64-DVD-1908.iso) is 4.2GB (1 hour at 1.7MB/s).

Once downloaded, make sure the ISO image is available in `$iso_path`. The first command, below, strips the `\r` from the `$iso_path` variable. If this does not work, then a manual intervention may be necessary (the `\r` causes difficulty with the existing implementation and stripping the carriage return is a workaround).

```bash
[root@sms-host vagrant]# iso_path=$(echo $iso_path | tr -d '\r')
[root@sms-host vagrant]# wget https://vault.centos.org/7.7.1908/isos/x86_64/CentOS-7-x86_64-DVD-1908.iso $iso_path --no-check-certificate

# !!! If the above does not work, run the following:
[root@sms-host vagrant]# iso_path=/vagrant/
[root@sms-host vagrant]# wget https://vault.centos.org/7.7.1908/isos/x86_64/CentOS-7-x86_64-DVD-1908.iso $iso_path --no-check-certificate
```

Ensure the ISO file is located in `$iso_path` and, if not, move it there.

> [!TIP]
> To monitor the progress of a long file transfer, screen multiplexers (like `screen` or `tmux`) can create a separate session to ‘watch’ the progress.
> 
> e.g. `watch "ls -lh /install/centos7.7/x86_64/"`

```bash
[root@sms-host vagrant]# mv CentOS-7-x86_64-DVD-1908.iso $iso_path
```

Initialise the image creation process using `copycds` - note the image is available in `${iso_path}` as per previous steps.

```bash
[root@sms-host vagrant]# copycds ${iso_path}/CentOS-7-x86_64-DVD-1908.iso  
```
Now several OS images should be available within xCAT.

```bash
# Query available images
[root@sms-host vagrant]# lsdef -t osimage
```
The above command should result in the following:

```text
centos7.7-x86_64-install-compute  (osimage)
centos7.7-x86_64-netboot-compute  (osimage)
centos7.7-x86_64-statelite-compute  (osimage)
```

This example is a stateless (PXE boot/_netboot_/RDBL) image for compute nodes. `genimage` initializes a **chroot-based** install.

```bash
# Save chroot location for compute image
[root@sms-host vagrant]# export CHROOT=/install/netboot/centos7.7/x86_64/compute/rootimg/

# Build initial chroot image
[root@sms-host vagrant]# genimage centos7.7-x86_64-netboot-compute 
```

### 3.2.1. Add OpenHPC components

> [!VIDEO]
> Start of video: [3.02B - HPC Ecosystems OpenHPC101 - Define compute image for provisioning](https://www.youtube.com/watch?v=HLFLM21EauQ&list=PL2s6Yr_Iu_ke16_di1C3dowXHF-hRLmC9&index=12)


After completion of the `genimage` process, we have a minimal CentOS7 configuration. For the Virtual Lab, we must add additional components to include resource management client services, and other additional packages to support the OpenHPC configuration. First, we enable the necessary package repositories for use *inside the **chroot***.

```bash
# Enable the package repositories inside the chroot
[root@sms-host vagrant]# yum-config-manager --installroot=$CHROOT --enable base  
[root@sms-host vagrant]# cp /etc/yum.repos.d/OpenHPC.repo $CHROOT/etc/yum.repos.d
[root@sms-host vagrant]# cp /etc/yum.repos.d/epel.repo $CHROOT/etc/yum.repos.d
```
Install base compute package (approx. 45MB).
```bash
# Install compute node base meta-package
[root@sms-host vagrant]# yum -y --installroot=$CHROOT install ohpc-base-compute  
```

Include additional components to the compute instance. `ohpc-slurm-client` is 34MB download size. Kernel is 123MB download size.

```bash
# Add Slurm client support meta-package
[root@sms-host vagrant]# yum -y --installroot=$CHROOT install ohpc-slurm-client

# Add Network Time Protocol (NTP) support
[root@sms-host vagrant]# yum -y --installroot=$CHROOT install ntp

# Add kernel drivers
[root@sms-host vagrant]# yum -y --installroot=$CHROOT install kernel

# Include modules user environment
[root@sms-host vagrant]# yum -y --installroot=$CHROOT install lmod-ohpc
```
### 3.2.2. Customise Compute Image System Configuration

OpenHPC offers a range of customisations that can be optionally applied to the local cluster. Although many of the features would be employed in a traditional HPC environment, there are some limitations in the Virtual Lab. Of the options available, this guide will include the following customizations:
  - Restrict ssh access to compute resources (PAM)
  - Nagios Core monitoring
  - Ganglia monitoring
  - ClusterShell
  - Node Health Check (NHC)

> [!TIP]
> We are installing multiple monitoring solutions to provide a broader overview of the available resources that can be viably installed in the Virtual Lab.

Enable NFS mounting of `$HOME` file system. Prior to the changes below, the NFS client mounts are configured as follows:

1) List the contents of the current `fstab` file for the `$CHROOT`

  ```bash
  # NFS client mounts before OpenHPC changes
  [root@sms-host vagrant]# cat $CHROOT/etc/fstab
  ```

  This command will list the current `fstab` configuration for the `$CHROOT` environment. The result should be as follows:

  ```text
  proc            /proc    proc   rw 0 0
  sysfs           /sys     sysfs  rw 0 0
  devpts          /dev/pts devpts rw,gid=5,mode=620 0 0
  ```

2) Add NFS client mounts of `/home` and `/opt/ohpc/pub` to the new base image

  ```bash
  [root@sms-host vagrant]# echo "${sms_ip}:/home /home nfs nfsvers=3,nodev,nosuid 0 0" >> $CHROOT/etc/fstab
  [root@sms-host vagrant]# echo "${sms_ip}:/opt/ohpc/pub /opt/ohpc/pub nfs nfsvers=3,nodev 0 0" >> $CHROOT/etc/fstab
  ```

3) Verify the NFS client mounts are listed AFTER changes:

  ```bash
  [root@sms-host vagrant]# cat $CHROOT/etc/fstab
  ```

  Now, you should see new lines referencing the NFS mounts.
    
    ```text
    proc            /proc    proc   rw 0 0
    sysfs           /sys     sysfs  rw 0 0
    devpts          /dev/pts devpts rw,gid=5,mode=620 0 0
    10.10.10.10:/home /home nfs nfsvers=3,nodev,nosuid 0 0
    10.10.10.10:/opt/ohpc/pub /opt/ohpc/pub nfs nfsvers=3,nodev 0 0
    ```

4) Export `/home` and OpenHPC public packages from the master server and enable/start the NFS service:

  ```bash
  [root@sms-host vagrant]# echo "/home *(rw,no_subtree_check,fsid=10,no_root_squash)" >> /etc/exports
  [root@sms-host vagrant]# echo "/opt/ohpc/pub *(ro,no_subtree_check,fsid=11)" >> /etc/exports
  [root@sms-host vagrant]# exportfs -a
  [root@sms-host vagrant]# systemctl restart nfs-server
  [root@sms-host vagrant]# systemctl enable nfs-server
  ```

5) Enable the NTP time service on computes and identify master host as the local NTP server:

  ```bash
  [root@sms-host vagrant]# chroot $CHROOT systemctl enable ntpd
  [root@sms-host vagrant]# echo "server ${sms_ip}" >> $CHROOT/etc/ntp.conf
  ```

### 3.2.3. Enable SSH control via Slurm

Restrict SSH access to compute nodes only for users who have an active job associated with the node using PAM..

```bash
# PAM restrict user access only to active job nodes
[root@sms-host vagrant]# echo "account required pam_slurm.so" >> $CHROOT/etc/pam.d/sshd
```

## 3.3. Add Nagios monitoring

> [!VIDEO]
> Start of video: [3.03 - HPC Ecosystems OpenHPC101 - Add Nagios Monitoring](https://www.youtube.com/watch?v=rWTdqYGr9Cg&list=PL2s6Yr_Iu_ke16_di1C3dowXHF-hRLmC9&index=13)

Nagios is an infrastructure monitoring package that monitors many infrastructure resources. `ohpc-nagios` is approx. 21MB download size.

1) Install Nagios meta-package on master host:

  ```bash
  [root@sms-host vagrant]# yum -y install ohpc-nagios  
  ```

2) Install plugins into compute node image:

  ```bash
  [root@sms-host vagrant]# yum -y --installroot=$CHROOT install nagios-plugins-all-ohpc nrpe-ohpc  
  ```

3) Enable and configure NRPE in compute image:

  ```bash
  [root@sms-host vagrant]# chroot $CHROOT systemctl enable nrpe  
  [root@sms-host vagrant]# perl -pi -e "s/^allowed_hosts=/# allowed_hosts=/" $CHROOT/etc/nagios/nrpe.cfg
  [root@sms-host vagrant]# echo "nrpe 5666/tcp # NRPE" >> $CHROOT/etc/services
  [root@sms-host vagrant]# echo "nrpe : ${sms_ip} : ALLOW" >> $CHROOT/etc/hosts.allow
  [root@sms-host vagrant]# echo "nrpe : ALL : DENY" >> $CHROOT/etc/hosts.allow
  [root@sms-host vagrant]# chroot $CHROOT /usr/sbin/useradd -c "NRPE user for the NRPE service" -d /var/run/nrpe -r -g nrpe -s /sbin/nologin nrpe

  # OUTPUT: useradd: user 'nrpe' already exists

  [root@sms-host vagrant]# chroot $CHROOT /usr/sbin/groupadd -r nrpe

  # OUTPUT: groupadd: group 'nrpe' already exists
  ```

4) Configure remote services to test on compute nodes:

  ```bash
  [root@sms-host vagrant]# mv /etc/nagios/conf.d/services.cfg.example /etc/nagios/conf.d/services.cfg
  ```

5) Define compute nodes as hosts to monitor:

  ```bash
  [root@sms-host vagrant]# mv /etc/nagios/conf.d/hosts.cfg.example /etc/nagios/conf.d/hosts.cfg
  ```

6) Strip `\r` from `$num_computes`:

  ```bash
  [root@sms-host vagrant]# num_computes=$(echo $num_computes | tr -d '\r')
  ```

7) Assign the correct hostname and IP addresses:

  > [!NOTICE]
  > While the OpenHPC guide offers a `for..do` loop to complete the next step, since we are only using two compute nodes, we will simply perform the simple set of commands twice. For completeness, the original `for..do` loop is included if you prefer to use it.

  ```bash
  [root@sms-host vagrant]# perl -pi -e "s/HOSTNAME1/${c_name[0]}/" /etc/nagios/conf.d/hosts.cfg
  [root@sms-host vagrant]# perl -pi -e "s/HOSTNAME2/${c_name[1]}/" /etc/nagios/conf.d/hosts.cfg
  [root@sms-host vagrant]# perl -pi -e "s/HOST1_IP/${c_ip[0]}/" /etc/nagios/conf.d/hosts.cfg
  [root@sms-host vagrant]# perl -pi -e "s/HOST2_IP/${c_ip[1]}/" /etc/nagios/conf.d/hosts.cfg
  ```

8) Update location of mail binary for alert commands:

  ```bash
  [root@sms-host vagrant]# perl -pi -e "s/ \/bin\/mail/ \/usr\/bin\/mailx/g" /etc/nagios/objects/commands.cfg
  ```

9) Update email address of contact for alerts:

  ```bash
  [root@sms-host vagrant]# perl -pi -e "s/nagios\@localhost/root\@${sms_name}/" /etc/nagios/objects/contacts.cfg  
  ```


> [!TIP]
> As a personal exercise, it can be useful to check the contents of a file before it is being modified to have an understanding of what this series of commands is doing. Using a screen multiplexer and a `watch` command can show live changes.

10) Check contents before change:

  ```bash
  [root@sms-host vagrant]# cat $CHROOT/etc/nagios/nrpe.cfg | grep check_ssh
  ```

11) Add check_ssh command for remote hosts:

  ```bash
  [root@sms-host vagrant]# echo command[check_ssh]=/usr/lib64/nagios/plugins/check_ssh localhost >> $CHROOT/etc/nagios/nrpe.cfg  
  ```

12) Check contents after change:

  ```bash
  [root@sms-host vagrant]# cat $CHROOT/etc/nagios/nrpe.cfg | grep check_ssh
  ```

13) Define password for nagiosadmin to be able to connect to web interface:

  ```bash
  # Adding password for user nagiosadmin
  [root@sms-host vagrant]# htpasswd -bc /etc/nagios/passwd nagiosadmin ${nagios_web_password}  
  ```

14) Enable Nagios on master, and configure:

  ```bash
  [root@sms-host vagrant]# systemctl enable nagios.service
  [root@sms-host vagrant]# systemctl start nagios  
  [root@sms-host vagrant]# chmod u+s `which ping`  
  ```

## 3.4. Add Ganglia Monitoring

>[!VIDEO]
>Start of video: [3.04 - HPC Ecosystems OpenHPC101 - Add Ganglia Monitoring](https://www.youtube.com/watch?v=njYX_HX9oQk&list=PL2s6Yr_Iu_ke16_di1C3dowXHF-hRLmC9&index=14)

Ganglia is a scalable distributed system monitoring tool for HPC systems. 

1) Install Ganglia meta-package on master:

  ```bash
  [root@sms-host vagrant]# yum -y install ohpc-ganglia  
  ```
2) Install Ganglia compute node daemon:

  ```bash
  [root@sms-host vagrant]# yum -y --installroot=$CHROOT install ganglia-gmond-ohpc
  ```

3) Use example configuration script to enable unicast receiver on master:

  ```bash
  [root@sms-host vagrant]# cp /opt/ohpc/pub/examples/ganglia/gmond.conf /etc/ganglia/gmond.conf
  ```

4) Check contents before and after:

  ```bash
  [root@sms-host vagrant]# cat /etc/ganglia/gmond.conf | grep sms

  # OUTPUT: host = <sms>

  [root@sms-host vagrant]# perl -pi -e "s/<sms>/${sms_name}/" /etc/ganglia/gmond.conf
  [root@sms-host vagrant]# cat /etc/ganglia/gmond.conf | grep sms

  # OUTPUT: host = sms-host
  ```

5) Add configuration to compute image and provide gridname:

  ```bash
  [root@sms-host vagrant]# cp /etc/ganglia/gmond.conf $CHROOT/etc/ganglia/gmond.conf  
  [root@sms-host vagrant]# echo "gridname MySite" >> /etc/ganglia/gmetad.conf  
  ```

6) Start and enable Ganglia services:

  ```bash
  [root@sms-host vagrant]# systemctl enable gmond  
  [root@sms-host vagrant]# systemctl enable gmetad  
  [root@sms-host vagrant]# systemctl start gmond  
  [root@sms-host vagrant]# systemctl start gmetad
  [root@sms-host vagrant]# chroot $CHROOT systemctl enable gmond

  # OUTPUT: 
  # Created symlink /etc/systemd/system/multi-user.target.wants/gmond.#service, pointing to /usr/lib/systemd/system/gmond.service.
  ```

7) Restart web server:

  ```bash
  [root@sms-host vagrant]# systemctl try-restart httpd  
  ```

> [!TIP]
> To test Ganglia monitoring we can run a rudimentary check on the SMS host with Lynx browser.
> ```bash
> # Install Lynx browser
> [root@sms-host vagrant]# yum install lynx -y
> [root@sms-host vagrant]# lynx http://localhost/ganglia
> ```

## 3.5. Add ClusterShell

>[!VIDEO]
> Start of video: 
[3.05 - HPC Ecosystems OpenHPC101 - Add ClusterShell](https://www.youtube.com/watch?v=EVqNo09SS1U&list=PL2s6Yr_Iu_ke16_di1C3dowXHF-hRLmC9&index=15)

ClusterShell executes commands in parallel across cluster nodes. This is a useful tool for testing and evaluating the Virtual Cluster.

1) Install ClusterShell:

  ```bash
  [root@sms-host vagrant]# yum -y install clustershell-ohpc
  ```

2) Setup node definitions:

  ```bash
  [root@sms-host vagrant]# cd /etc/clustershell/groups.d
  [root@sms-host groups.d]# mv local.cfg local.cfg.orig
  [root@sms-host groups.d]# echo "adm: ${sms_name}" > local.cfg
  [root@sms-host groups.d]# echo "compute: compute00" >> local.cfg
  [root@sms-host groups.d]# echo "compute: compute01" >> local.cfg
  [root@sms-host groups.d]# echo "all: @adm,@compute" >> local.cfg
  ```

  Looking inside the above mentioned file using `cat local.cfg` should reveal the following:
  ```text
  adm: sms-host
  compute: compute00
  compute: compute01
  all: @adm,@compute
  ```

## 3.6. Add NHC (Node Health Check)

>[!VIDEO]
>Start of video:
[3.06 - HPC Ecosystems OpenHPC101 - NHC (Node Health Check)](https://www.youtube.com/watch?v=7cTgQVRSrv8&list=PL2s6Yr_Iu_ke16_di1C3dowXHF-hRLmC9&index=16)

Node Health Check runs a periodic check on each compute node to verify that the node is in good working order. This is a useful experimental tool for the Virtual Lab.

1) Install NHC on master and compute nodes:

  ```bash
  [root@sms-host vagrant]# yum -y install nhc-ohpc
  [root@sms-host vagrant]# yum -y --installroot=$CHROOT install nhc-ohpc
  ```

2) Register as Slurm's health check program:

  ```bash
  [root@sms-host vagrant]# echo "HealthCheckProgram=/usr/sbin/nhc" >> /etc/slurm/slurm.conf
  [root@sms-host vagrant]# echo "HealthCheckInterval=300" >> /etc/slurm/slurm.conf # execute every five minutes
  ```

## 3.7. Identify files for synchronization

> [!VIDEO]
> Start of video: [3.07 - HPC Ecosystems OpenHPC101 - Identify files for synchronization xCAT](https://www.youtube.com/watch?v=LxVJVJTKGuE&list=PL2s6Yr_Iu_ke16_di1C3dowXHF-hRLmC9&index=17)

xCAT includes functionality to synchronize files from the SMS host to managed compute nodes. The default OpenHPC method for user credential management is to distribute user credentials to the compute nodes.

> [!TIP]
> Many systems implement a protocol such as LDAP as a central authentication service - the Virtual Lab adopts the default OpenHPC method of distributed user credentials to illustrate both the simplicity and the flexibility of this method.

1) Define path for xCAT synclist file:

  ```bash
  [root@sms-host vagrant]# mkdir -p /install/custom/netboot  
  [root@sms-host vagrant]# chdef -t osimage -o centos7.7-x86_64-netboot-compute synclists="/install/custom/netboot/compute.synclist"

  # OUTPUT: 
  # 1 object definitions have been created or modified.
  # New object definitions 'centos7.7-x86_64-netboot-compute' have been created.
  ```

2) Add desired credential files to synclist:

  ```bash
  [root@sms-host vagrant]# echo "/etc/passwd -> /etc/passwd" > /install/custom/netboot/compute.synclist
  [root@sms-host vagrant]# echo "/etc/group -> /etc/group" >> /install/custom/netboot/compute.synclist
  [root@sms-host vagrant]#  echo "/etc/shadow -> /etc/shadow" >> /install/custom/netboot/compute.synclist
  ```

  > [!TIP]
  > `/install/custom/netboot/compute.synclist` contains a list of all files to be distributed from the host to the target **compute** machines

3) We follow the same process to import the global Slurm configuration file and the cryptographic key for munge authentication library. Add desired files to synclist:

  ```bash
  [root@sms-host vagrant]# echo "/etc/slurm/slurm.conf -> /etc/slurm/slurm.conf " >> /install/custom/netboot/compute.synclist  
  [root@sms-host vagrant]# echo "/etc/munge/munge.key -> /etc/munge/munge.key " >> /install/custom/netboot/compute.synclist  
  ```

4) Synchronize files to compute nodes:

  ```bash
  [root@sms-host vagrant]# updatenode compute -F

  # OUTPUT: File synchronization has completed for nodes: "compute00,compute01"
  ```
  
> [!TIP]
> As per the OpenHPC guide:
“The `updatenode compute -F` command can be used to distribute changes made to any defined synchronization files on the SMS host. Users wishing to automate this process may want to consider adding a `crontab` entry to perform this action at defined intervals.”

> [!CONGRATS]
> You have concluded the basic setup of xCAT on the SMS host.

> [!RECAP]
> In this Chapter the Virtual Cluster components were deployed:
>
> - **Compute images** - used by the stateless compute nodes.
>
> - **Nagios monitoring** - this will provide basic metrics and analytics of the cluster.
>
> - **Ganglia monitoring** - this will provide basic metrics and analytics of the cluster.
>
> - **ClusterShell** - used to execute commands in parallel across cluster nodes - very useful for testing and evaluating the functionality of the cluster.
>
> - **Node Health Check (NHC)** - runs a periodic check on each compute node to verify that the node is in good working order.
>
> - **Identifying files for synchronization by xCAT** - synchronizing base files from the SMS host to the managed compute nodes.
