# Quick Links

## Videos

The videos contained in the youtube playlists below were created to supplement this guide. The videos are numbered in a manner that maps to the document chapters and section numbers.

[HPC Ecosystems OpenHPC101 - Series 1 - YouTube](https://www.youtube.com/playlist?list=PL2s6Yr_Iu_ke16_di1C3dowXHF-hRLmC9)

[HPC Ecosystems OpenHPC101 - Series 2 - YouTube](https://www.youtube.com/playlist?list=PL2s6Yr_Iu_kfOmhsxBSstz00p10uOo9eY)

## Official OpenHPC PDF Guide

An official OpenHPC PDF Guide is provided by the OpenHPC community as a standalone guide for deploying the OpenHPC software suite on physical hardware. However, to ensure parity across all HPC Ecosystems community installations, **the HPC Ecosystems Project Team recommends that all community members follow this HOW TO guide** (the one you are reading) instead of the PDF guide below.

Of course, if you (and your team) are comfortable with following the generic instructions provided by the OpenHPC community, you can reference the official PDF guide below. Note, however, that there is a high likelihood that your deployment of OpenHPC will deviate in some way from the HPC Ecosystems Project’s supported instance of OpenHPC.

https://github.com/openhpc/ohpc/releases/download/v1.3.9.GA/Install_guide-CentOS7-xCAT-Stateless-SLURM-1.3.9-x86_64.pdf

