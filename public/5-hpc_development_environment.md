# 5. HPC Development Environment

## 5.0. Introduction to Chapter 

The install procedure outlined in [§3 - xCAT Setup](3-xcat-setup.md) highlighted the steps necessary to install a master host, assemble and customize a compute image, and provision several compute hosts from bare-metal. With these steps completed, additional OpenHPC-provided packages can now be added to support a flexible HPC development environment including development tools, C/C++/FORTRAN compilers, MPI stacks, and a variety of 3rd party libraries. The following subsections highlight the additional software installation procedures.

## 5.1. Install OpenHPC Development Components

> [!VIDEO]
> Start of video: [5.01 - Install OpenHPC Development Components](https://www.youtube.com/watch?v=HLVIJnH912U&list=PL2s6Yr_Iu_kfOmhsxBSstz00p10uOo9eY&index=6)


Install autotools meta-package:
```bash
[root@sms-host vagrant]# yum -y install ohpc-autotools
[root@sms-host vagrant]# yum -y install EasyBuild-ohpc  
[root@sms-host vagrant]# yum -y install hwloc-ohpc  
[root@sms-host vagrant]# yum -y install spack-ohpc  
[root@sms-host vagrant]# yum -y install valgrind-ohpc
```

## 5.2. Compilers

>[!VIDEO]
>Start of video: [5.02 - Compilers](https://www.youtube.com/watch?v=NW8bUoV8cww&list=PL2s6Yr_Iu_kfOmhsxBSstz00p10uOo9eY&index=7)

Install GNU8 and LLVM5 compilers:
```bash
[root@sms-host vagrant]# yum -y install gnu8-compilers-ohpc
[root@sms-host vagrant]# yum -y install llvm5-compilers-ohpc
```

## 5.3. MPI Stacks

>[!VIDEO]
> Start of video: [5.03 - MPI Stacks](https://www.youtube.com/watch?v=-EepSmVJn0c&list=PL2s6Yr_Iu_kfOmhsxBSstz00p10uOo9eY&index=8)

Install OpenMPI and MPICH:

```bash
[root@sms-host vagrant]# yum -y install openmpi3-gnu8-ohpc mpich-gnu8-ohpc  
```

## 5.4. Performance Tools

>[!VIDEO]
>Start of video: [5.04 - Performance Tools](https://www.youtube.com/watch?v=WX9SY4Gjhhk&list=PL2s6Yr_Iu_kfOmhsxBSstz00p10uOo9eY&index=9)

Install perf-tools meta-package:
```bash
[root@sms-host vagrant]# yum -y install ohpc-gnu8-perf-tools
```

## 5.5. Set up default development environment

>[!VIDEO]
>Start of video: [5.05 - Set Up Default Development Environment](https://www.youtube.com/watch?v=t3YGSXW1sJI&list=PL2s6Yr_Iu_kfOmhsxBSstz00p10uOo9eY&index=10)

```bash
[root@sms-host vagrant]# yum -y install lmod-defaults-gnu8-openmpi3-ohpc
```
